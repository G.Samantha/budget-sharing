import './style.css';
import { GroupFormComponent } from './components/GroupFormComponent.js';
import { ExpenseFormComponent } from './components/ExpenseFormComponent.js';
// import { SummaryComponent } from './components/SummaryComponent.js';
import { ExpenseListComponent } from './components/ExpenseListComponent.js';
import { ExpenseGroup } from './expenseGroup.js';
import { ListGroupComponent } from './components/ListGroupComponent.js';
import { Expense } from './expense.js';
import { Person } from './person.js';

export const groupFormContainer = document.querySelector<HTMLDivElement>('#divGroupForm');
export const groupListContainer = document.querySelector<HTMLDivElement>('#divGroupList');
export const expenseFormContainer = document.querySelector<HTMLDivElement>('#divExpenseForm');
export const expenseListContainer = document.querySelector<HTMLDivElement>('#divExpenseList');

let groupList: ExpenseGroup[] = [];
let groupListComponent: ListGroupComponent;
let expenseFormComponent: ExpenseFormComponent;
let expenseListComponent: ExpenseListComponent;
let selectedGroup: ExpenseGroup;

function onGroupCreation(groupName: string) {
    const id = groupList.length;
    const group = new ExpenseGroup(id, groupName, [], false);
    groupList.push(group);
    groupListComponent.render();
}

function onExpenseCreation(expenseData: any) {
    const person = new Person(selectedGroup.expenses.length, expenseData.personName);
    const expense = new Expense(selectedGroup.expenses.length, expenseData.description, expenseData.amount, person, false);
    selectedGroup.expenses.push(expense);
    renderExpenseBlock();
}

function onGroupClick(index: number): void {
    if (index >= 0 && index < groupList.length) {
        selectedGroup = groupList[index];
        groupListComponent.setSelectedGroup(selectedGroup);
        groupListComponent.render();
        renderExpenseBlock();
    }
}

function renderExpenseBlock(): void {
    if (!expenseListComponent || !expenseFormComponent) {
        if (expenseFormContainer && expenseListContainer) {
            expenseListComponent = new ExpenseListComponent(expenseListContainer, selectedGroup.expenses);
            expenseFormComponent = new ExpenseFormComponent(expenseFormContainer, onExpenseCreation);
        }
    }
    expenseFormComponent.render();
    expenseListComponent.render();
}

document.addEventListener("DOMContentLoaded", () => {
    if (groupFormContainer) {
        // Formulaire ajout groupe
        const groupFormComponent = new GroupFormComponent(groupFormContainer, onGroupCreation);
        groupFormComponent.render();

        // Rendu liste groupe
        if (groupListContainer) {
            groupListComponent = new ListGroupComponent(groupListContainer, groupList, selectedGroup, onGroupClick);
            groupListComponent.render();
        }

        // TODO : Balance component + bouton
    }
});
