import { Expense } from "./expense";

export class ExpenseGroup {
    id: number;
    groupName: string;
    expenses: Expense[];
    disabled: boolean;

    constructor(id: number, groupName: string, expenses: Expense[], disabled: boolean) {
        this.id = id;
        this.groupName = groupName;
        this.expenses = expenses;
        this.disabled = disabled;
    }

    addExpense(expense: Expense) {
        this.expenses.push(expense);
    }

    updateExpense(expense: Expense) {
        const index = this.expenses.findIndex(exp => exp.id === expense.id);
        if (index !== -1) {
            this.expenses[index] = expense;
        }
    }

    deleteExpense(expense: Expense) {
        const index = this.expenses.findIndex(exp => exp.id === expense.id);
        if (index !== -1) {
            this.expenses[index].disableExpense();
        }
    }

    disableGroup() {
        this.disabled = true;
    }

    getGroupName(): string {
        return this.groupName;
    }

    getExpenses(): Expense[] {
        return this.expenses;
    }

    isDisabled(): boolean {
        return this.disabled;
    }

    // viewBudget() {
    //     return this.budgets;
    // }

    // calculateTotalBudgets() {
    //     return this.budgets.reduce((total, budget) => total + budget.price, 0);
    // }

    // calculateTotalPerPerson() {
    //     return this.calculateTotalBudgets() / this.persons.length;
    // }
}
