import { Person } from "./person";

export interface ExpenseInterface {
    id: number;
    name: string;
    price: number;
    buyer: Person;
    disabled: boolean;
}

export class Expense implements ExpenseInterface {
    id: number;
    name: string;
    price: number;
    buyer: Person;
    disabled: boolean;

    constructor(id: number, name: string, price: number, buyer: Person, disabled: boolean) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.buyer = buyer;
        this.disabled = disabled;
    }

    getName(): string {
        return this.name;
    }

    getPrice(): number {
        return this.price;
    }

    getBuyer(): Person {
        return this.buyer;
    }

    disableExpense() {
        this.disabled = true;
    }

    isDisabled(): boolean {
        return this.disabled;
    }
}