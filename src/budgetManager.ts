// import { BudgetInterface } from "./expense";
// import { BudgetGroup } from "./expenseGroup";

// import { PersonInterface } from "./person";

// class BudgetManager {
//     private budgetGroup: BudgetGroup;

//     constructor() {
//         this.budgetGroup = new BudgetGroup();
//     }

//     createGroup(groupName: string) {
//         this.budgetGroup.createGroup(groupName);
//     }

//     addBudget(description: string, amount: number, person: string) {
//         const budget: BudgetInterface = { name: description, price: amount };
//         this.budgetGroup.addBudget(budget);

//         const personData: PersonInterface = { name: person, budget: budget };
//         this.budgetGroup.addPerson(personData);
//     }

//     updateBudgetList() {
//         const budgetList = document.getElementById("budget-list"); 
    
//         if (budgetList) {
//             budgetList.innerHTML = ""; 
//             const budgets = this.budgetGroup.viewBudget(); 
    
//             budgets.forEach(budget => {
//                 const listItem = document.createElement("li");
//                 listItem.textContent = `${budget.name} - ${budget.price}€`;
//                 budgetList.appendChild(listItem); 
//             });
//         }
//     }
    
//     closeGroup() {
//         const totalBudgetsElement = document.getElementById("total");
//         const totalPerPersonElement = document.getElementById("total par personne");
    
//         if (totalBudgetsElement && totalPerPersonElement) {
//             const totalBudgets = this.budgetGroup.calculateTotalBudgets(); 
//             const totalPerPerson = this.budgetGroup.calculateTotalPerPerson(); 
    
//             totalBudgetsElement.textContent = totalBudgets.toFixed(2) + "€"; 
//             totalPerPersonElement.textContent = totalPerPerson.toFixed(2) + "€"; 
//         }
//     }
    
// }

// export const budgetManager = new BudgetManager();
