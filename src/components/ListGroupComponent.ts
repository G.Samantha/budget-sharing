import { ExpenseGroup } from "../expenseGroup";

export class ListGroupComponent {
    container: HTMLElement;
    groupList: ExpenseGroup[];
    selectedGroup: ExpenseGroup |null;
    onGroupClick: (index: number) => void;

    constructor(container: HTMLElement, groupList: ExpenseGroup[], selectedGroup: ExpenseGroup | null, onGroupClick: (index: number) => void) {
        this.container = container;
        this.groupList = groupList;
        this.selectedGroup = selectedGroup;
        this.onGroupClick = onGroupClick;
    }

    setSelectedGroup(expenseGroup: ExpenseGroup) {
        this.selectedGroup = expenseGroup;
    }

    render() {
        this.container.innerHTML = '';
        const groupListContainer = document.createElement("div");
        const groupNameLabel = document.createElement("span");
        groupNameLabel.innerText = 'Liste des groupes';
        groupListContainer.appendChild(groupNameLabel);
        
        for (let i = 0; i < this.groupList.length; i++) {
            const groupContainer = document.createElement("div");
            if (this.selectedGroup && this.selectedGroup !== null) {
                if (this.selectedGroup !== null && this.groupList[i].id === this.selectedGroup.id) {
                    groupContainer.style.border = "2px solid red";
                } else {
                    groupContainer.style.border = "2px solid black";
                }
            } else {
                groupContainer.style.border = "2px solid black";
            }
            groupContainer.innerHTML = this.groupList[i].groupName;
            groupContainer.addEventListener('click', () => {
                if (this.onGroupClick) {
                    this.onGroupClick(i);
                }
            });
            groupListContainer.appendChild(groupContainer); 
        }
        
        this.container.appendChild(groupListContainer);
    }
}