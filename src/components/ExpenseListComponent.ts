import { Expense } from "../expense";

export class ExpenseListComponent {
    container: HTMLElement;
    expenseList: Expense[];

    constructor(container: HTMLElement, expenseList: Expense[]) {
        this.container = container;
        this.expenseList = expenseList;
    }

    render() {
        this.container.innerHTML = '';
        const expenseListContainer = document.createElement("div");
        const expenseListLabel = document.createElement("span");
        expenseListLabel.innerText = 'Liste des dépenses';
        expenseListContainer.appendChild(expenseListLabel);
        
        for (let i = 0; i < this.expenseList.length; i++) {
            const expenseContainer = document.createElement("div");
            expenseContainer.style.border = "2px solid black";
            const expenseBuyer = document.createElement("span");
            expenseBuyer.innerHTML = this.expenseList[i].buyer.name + " | ";
            const expenseDescription = document.createElement("span");
            expenseDescription.innerHTML = this.expenseList[i].name + " = ";
            const expenseAmount = document.createElement("span");
            expenseAmount.innerHTML = this.expenseList[i].price.toString() + "€";
            expenseContainer.appendChild(expenseBuyer);
            expenseContainer.appendChild(expenseDescription);
            expenseContainer.appendChild(expenseAmount);
            expenseListContainer.appendChild(expenseContainer); 
        }
        
        this.container.appendChild(expenseListContainer);
    }
}
