import { closeGroup } from "../app";


export class CloseGroupButtonComponent {
    constructor(private container: HTMLElement) {}

    render() {
        const closeGroupButton = document.createElement("button");
        closeGroupButton.textContent = "Clôturer le groupe";
        closeGroupButton.addEventListener("click", () => {
            closeGroup();
        });

        this.container.appendChild(closeGroupButton);
    }
}
