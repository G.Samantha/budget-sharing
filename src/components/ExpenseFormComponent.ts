export class ExpenseFormComponent {
    container: HTMLElement;
    onExpenseCreation: Function;

    constructor(container: HTMLElement, onExpenseCreation: Function) {
        this.container = container;
        this.onExpenseCreation = onExpenseCreation;
    }

    render() {
        this.container.innerHTML = '';
        const expenseForm = document.createElement("div");
        expenseForm.innerHTML = `
            <h2>Entrer des dépenses</h2>
                <div>
                    <label for="person-name">Prénom:</label>
                    <input type="text" id="person-name">
                </div>

                <div>
                    <label for="expense-description">Description:</label>
                    <input type="text" id="expense-description">
                </div>

                <div>
                <label for="expense-amount">Montant (€) :</label>
                <input type="number" id="expense-amount">
                </div>

                <div>
                    <button id="add-expense-button">Ajouter une dépense</button>
                </div>
        `;

        const addExpenseButton = expenseForm.querySelector("#add-expense-button") as HTMLButtonElement;
        const descriptionElement = expenseForm.querySelector("#expense-description") as HTMLInputElement;
        const amountElement = expenseForm.querySelector("#expense-amount") as HTMLInputElement;
        const personNameElement = expenseForm.querySelector("#person-name") as HTMLInputElement;

        if (addExpenseButton && descriptionElement && amountElement && personNameElement) {
            addExpenseButton.addEventListener("click", () => {
                const description = descriptionElement.value;
                const amount = parseFloat(amountElement.value);
                const personName = personNameElement.value;

                if (description && !isNaN(amount) && amount > 0 && personName) {
                    this.onExpenseCreation({description, amount, personName});

                    descriptionElement.value = "";
                    amountElement.value = "";
                    personNameElement.value = "";
                }
            });
        }

        this.container.appendChild(expenseForm);
    }
}
