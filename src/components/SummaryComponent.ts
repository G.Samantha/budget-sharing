import { CloseGroupButtonComponent } from './CloseGroupButtonComponent';

export class SummaryComponent {
    constructor(private container: HTMLElement) {}

    render() {
        const summary = document.createElement("div");
        summary.innerHTML = `
            <h2>Résumé du groupe de dépenses</h2>
            <p>Total des dépenses: <span id="total-expenses">0</span>€</p>
            <p>Total par personne: <span id="total-per-person">0</span>€</p>
        `;

        const closeGroupButtonComponent = new CloseGroupButtonComponent(summary);
        closeGroupButtonComponent.render();

        this.container.appendChild(summary);
    }
}
