export class GroupFormComponent {
    container: HTMLElement;
    onGroupCreation: Function;
    
    constructor(container: HTMLElement, onGroupCreation: Function) {
        this.container = container;
        this.onGroupCreation = onGroupCreation;
    }

    render() {
        const groupForm = document.createElement("div");
        groupForm.innerHTML = `
            <h2>Créer un groupe de dépenses</h2>
            <label for="group-name">Nom du groupe:</label>
            <input type="text" id="group-name">
        `;

        const createGroupButton = document.createElement('button');
        createGroupButton.textContent = 'Créer un groupe';
        createGroupButton.addEventListener('click', () => {
            const groupName = document.getElementById('group-name')?.value;
            if (groupName) {
                this.onGroupCreation(groupName);
            } else {
                alert("rentre un nom konar");
            }
        });

        groupForm.appendChild(createGroupButton);

        this.container.appendChild(groupForm);
    }
}
